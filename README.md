# Seq2Species: Neural Network Models for Species Classification
## This fork is adopted to my thesis work, see below for original readme
## To reproduce for python 3 error
1. Create a new python3 venv and activate it
2. Do `pip install tensorflow==1.15`
3. link author data `ln -s  /lustre/BIF/nobackup/grosm002/seq2species_data/ author_data`
3. run `./train_NN_on_author_data.sh`
4. Get the error

<details>
  <summary>Full Error here</summary>
  

```
(venv_p3) bart@Bart-HP-PAV14:~/seq2species$ ./train_NN_on_author_data.sh 
Current Hyperparameters:
         use_depthwise_separable :  true
         filter_widths :  [5, 9, 13]
         filter_depths :  [1, 1, 1]
         pointwise_depths :  [84, 58, 180]
         num_fc_layers :  2
         num_fc_units :  2828
         min_read_length :  100
         pooling_type :  avg
         lrelu_slope :  0
         keep_prob :  0.94017831318
         weight_scale :  1.18409526348
         grad_clip_norm :  20.0
         lr_init :  100.0
         lr_decay :  0
         optimizer :  adam
         optimizer_hp :  0.885769367218
         train_steps :  3000000
Constructing TensorFlow Graph.
Starting model training.
WARNING:tensorflow:
The TensorFlow contrib module will not be included in TensorFlow 2.0.
For more information, please see:
  * https://github.com/tensorflow/community/blob/master/rfcs/20180907-contrib-sunset.md
  * https://github.com/tensorflow/addons
  * https://github.com/tensorflow/io (for I/O related ops)
If you depend on functionality not listed there, please file an issue.

W0416 11:14:00.832121 139847438759744 lazy_loader.py:50] 
The TensorFlow contrib module will not be included in TensorFlow 2.0.
For more information, please see:
  * https://github.com/tensorflow/community/blob/master/rfcs/20180907-contrib-sunset.md
  * https://github.com/tensorflow/addons
  * https://github.com/tensorflow/io (for I/O related ops)
If you depend on functionality not listed there, please file an issue.

SER EXAMPLE Tensor("args_0:0", shape=(), dtype=string)
GOT SEQ Tensor("parse_single_tfexample/ParseSingleExample/ParseSingleExample:0", shape=(), dtype=string)
WARNING:tensorflow:From /home/bart/seq2species/input.py:187: py_func (from tensorflow.python.ops.script_ops) is deprecated and will be removed in a future version.
Instructions for updating:
tf.py_func is deprecated in TF V2. Instead, there are two
    options available in V2.
    - tf.py_function takes a python function which manipulates tf eager
    tensors instead of numpy arrays. It's easy to convert a tf eager tensor to
    an ndarray (just call tensor.numpy()) but having access to eager tensors
    means `tf.py_function`s can use accelerators such as GPUs as well as
    being differentiable using a gradient tape.
    - tf.numpy_function maintains the semantics of the deprecated tf.py_func
    (it is not differentiable, and manipulates numpy arrays). It drops the
    stateful argument making all functions stateful.
    
W0416 11:14:02.247298 139847438759744 deprecation.py:323] From /home/bart/seq2species/input.py:187: py_func (from tensorflow.python.ops.script_ops) is deprecated and will be removed in a future version.
Instructions for updating:
tf.py_func is deprecated in TF V2. Instead, there are two
    options available in V2.
    - tf.py_function takes a python function which manipulates tf eager
    tensors instead of numpy arrays. It's easy to convert a tf eager tensor to
    an ndarray (just call tensor.numpy()) but having access to eager tensors
    means `tf.py_function`s can use accelerators such as GPUs as well as
    being differentiable using a gradient tape.
    - tf.numpy_function maintains the semantics of the deprecated tf.py_func
    (it is not differentiable, and manipulates numpy arrays). It drops the
    stateful argument making all functions stateful.
    
WARNING:tensorflow:From /home/bart/seq2species/input.py:141: calling string_split (from tensorflow.python.ops.ragged.ragged_string_ops) with delimiter is deprecated and will be removed in a future version.
Instructions for updating:
delimiter is deprecated, please use sep instead.
W0416 11:14:02.384907 139847438759744 deprecation.py:506] From /home/bart/seq2species/input.py:141: calling string_split (from tensorflow.python.ops.ragged.ragged_string_ops) with delimiter is deprecated and will be removed in a future version.
Instructions for updating:
delimiter is deprecated, please use sep instead.
READ FEAT {'sequence': <tf.Tensor 'parse_single_tfexample/encode_read/Cast:0' shape=(?, 4) dtype=float32>}
LABEL {'species': <tf.Tensor 'parse_single_tfexample/encode_label/species/Cast:0' shape=() dtype=int32>}
WARNING:tensorflow:From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/data/util/random_seed.py:58: where (from tensorflow.python.ops.array_ops) is deprecated and will be removed in a future version.
Instructions for updating:
Use tf.where in 2.0, which has the same broadcast rule as np.where
W0416 11:14:02.476253 139847438759744 deprecation.py:323] From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/data/util/random_seed.py:58: where (from tensorflow.python.ops.array_ops) is deprecated and will be removed in a future version.
Instructions for updating:
Use tf.where in 2.0, which has the same broadcast rule as np.where
INFO:tensorflow:Create CheckpointSaverHook.
I0416 11:14:03.240410 139847438759744 basic_session_run_hooks.py:541] Create CheckpointSaverHook.
WARNING:tensorflow:From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/training_util.py:236: Variable.initialized_value (from tensorflow.python.ops.variables) is deprecated and will be removed in a future version.
Instructions for updating:
Use Variable.read_value. Variables in 2.X are initialized automatically both in eager and graph (inside tf.defun) contexts.
W0416 11:14:03.381304 139847438759744 deprecation.py:323] From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/training_util.py:236: Variable.initialized_value (from tensorflow.python.ops.variables) is deprecated and will be removed in a future version.
Instructions for updating:
Use Variable.read_value. Variables in 2.X are initialized automatically both in eager and graph (inside tf.defun) contexts.
INFO:tensorflow:Graph was finalized.
I0416 11:14:03.476156 139847438759744 monitored_session.py:240] Graph was finalized.
2020-04-16 11:14:03.499676: E tensorflow/stream_executor/cuda/cuda_driver.cc:318] failed call to cuInit: UNKNOWN ERROR (303)
INFO:tensorflow:Restoring parameters from logdir/model.ckpt-0
I0416 11:14:03.503978 139847438759744 saver.py:1284] Restoring parameters from logdir/model.ckpt-0
WARNING:tensorflow:From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/saver.py:1069: get_checkpoint_mtimes (from tensorflow.python.training.checkpoint_management) is deprecated and will be removed in a future version.
Instructions for updating:
Use standard file utilities to get mtimes.
W0416 11:14:07.253412 139847438759744 deprecation.py:323] From /home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/saver.py:1069: get_checkpoint_mtimes (from tensorflow.python.training.checkpoint_management) is deprecated and will be removed in a future version.
Instructions for updating:
Use standard file utilities to get mtimes.
INFO:tensorflow:Running local_init_op.
I0416 11:14:07.587593 139847438759744 session_manager.py:500] Running local_init_op.
INFO:tensorflow:Done running local_init_op.
I0416 11:14:07.816322 139847438759744 session_manager.py:502] Done running local_init_op.
INFO:tensorflow:Saving checkpoints for 0 into logdir/model.ckpt.
I0416 11:14:08.641824 139847438759744 basic_session_run_hooks.py:606] Saving checkpoints for 0 into logdir/model.ckpt.
Traceback (most recent call last):
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1365, in _do_call
    return fn(*args)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1350, in _run_fn
    target_list, run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1443, in _call_tf_sessionrun
    run_metadata)
tensorflow.python.framework.errors_impl.InvalidArgumentError: {{function_node __inference_Dataset_map__InputEncoding.parse_single_tfexample_55}} indices[11] = -1 is not in [0, 16)
         [[{{node parse_single_tfexample/encode_read/GatherV2}}]]
         [[IteratorGetNext]]

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "run_training.py", line 296, in <module>
    tf.compat.v1.app.run(main)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/platform/app.py", line 40, in run
    _run(main=main, argv=argv, flags_parser=_parse_flags_tolerate_undef)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/absl/app.py", line 299, in run
    _run_main(main, args)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/absl/app.py", line 250, in _run_main
    sys.exit(main(argv))
  File "run_training.py", line 288, in main
    for cur_measures, cur_file in run_training(model, hparams, input_dataset, FLAGS.logdir, batch_size=FLAGS.batch_size):
  File "run_training.py", line 159, in run_training
    global_step = sess.run(model.global_step)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 754, in run
    run_metadata=run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 1259, in run
    run_metadata=run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 1360, in run
    raise six.reraise(*original_exc_info)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/six.py", line 703, in reraise
    raise value
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 1345, in run
    return self._sess.run(*args, **kwargs)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 1418, in run
    run_metadata=run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/training/monitored_session.py", line 1176, in run
    return self._sess.run(*args, **kwargs)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 956, in run
    run_metadata_ptr)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1180, in _run
    feed_dict_tensor, options, run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1359, in _do_run
    run_metadata)
  File "/home/bart/seq2species/venv_p3/lib/python3.7/site-packages/tensorflow_core/python/client/session.py", line 1384, in _do_call
    raise type(e)(node_def, op, message)
tensorflow.python.framework.errors_impl.InvalidArgumentError:  indices[11] = -1 is not in [0, 16)
         [[{{node parse_single_tfexample/encode_read/GatherV2}}]]
         [[IteratorGetNext]]
(venv_p3) bart@Bart-HP-PAV14:~/seq2species$
```
</details>

# Original readme

*A deep learning solution for read-level taxonomic classification with 16s.*

Recent improvements in sequencing technology have made possible large, public
databases of biological sequencing data, bringing about new data richness for
many important problems in bioinformatics. However, this growing availability of
data creates a need for analysis methods capable of efficiently handling these
large sequencing datasets. We on the [Genomics team in Google
Brain](https://ai.google/research/teams/brain/healthcare-biosciences) are
particularly interested in the class of problems which can be framed as
assigning meaningful labels to short biological sequences, and are exploring the
possiblity of creating a general deep learning solution for solving this class
of sequence-labeling problems. We are excited to share our initial progress in
this direction by releasing Seq2Species, an open-source neural network framework
for [TensorFlow](https://www.tensorflow.org/) for predicting read-level
taxonomic labels from genomic sequence. Our release includes all the code
necessary to train new Seq2Species models.

## About Seq2Species

Briefly, Seq2Species provides a framework for training deep neural networks to
predict database-derived labels directly from short reads of DNA. Thus far, our
research has focused predominantly on demonstrating the value of this deep
learning approach on the problem of determining the species of origin of
next-generation sequencing reads from [16S ribosomal
DNA](https://en.wikipedia.org/wiki/16S_ribosomal_RNA). We used this
Seq2Species framework to train depthwise separable convolutional neural networks
on short subsequences from the 16S genes of more than 13 thousand distinct
species. The resulting classification model assign species-level probabilities
to individual 16S reads.

For more information about the use cases we have explored, or for technical
details describing how Seq2Species work, please see our
[preprint](https://www.biorxiv.org/content/early/2018/06/22/353474).

## Installation

Training Seq2Species models requires installing the following dependencies:

* python 2.7

* protocol buffers

* numpy

* absl

### Dependencies

Detailed instructions for installing TensorFlow are available on the [Installing
TensorFlow](https://www.tensorflow.org/install/) website. Please follow the
full instructions for installing TensorFlow with GPU support. For most
users, the following command will suffice for continuing with CPU support only:
```bash
# For CPU
pip install --upgrade tensorflow
```

The TensorFlow installation should also include installation of the numpy and
absl libraries, which are two of TensorFlow's python dependencies. If
necessary, instructions for standalone installation are available:

* [numpy](https://scipy.org/install.html)

* [absl](https://github.com/abseil/abseil-py)

Information about protocol buffers, as well as download and installation
intructions for the protocol buffer (protobuf) compiler, are available on the [Google
Developers website](https://developers.google.com/protocol-buffers/). A typical
Ubuntu user can install this library using `apt-get`:
```bash
sudo apt-get install protobuf-compiler
```

### Clone

Now, clone `tensorflow/models` to start working with the code:
```bash
git clone https://github.com/tensorflow/models.git
```

### Protobuf Compilation

Seq2Species uses protobufs to store and save dataset and model metadata. Before
the framework can be used to build and train models, the protobuf libraries must
be compiled. This can be accomplished using the following command:
```bash
# From tensorflow/models/research
protoc seq2species/protos/seq2label.proto --python_out=.
```

### Testing the Installation

One can test that Seq2Species has been installed correctly by running the
following command:
```bash
python seq2species/run_training_test.py
```

## Usage Information

Input data to Seq2Species models should be [tf.train.Example protocol messages](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/core/example/example.proto) stored in
[TFRecord format](https://www.tensorflow.org/versions/r1.0/api_guides/python/python_io#tfrecords_format_details).
Specifically, the input pipeline expects tf.train.Examples with a 'sequence' field
containing a genomic sequence as an upper-case string, as one field for each
target label (e.g. 'species'). There should also be an accompanying
Seq2LabelDatasetInfo text protobuf containing metadata about the input, including
the possible label values for each target.

Below, we give an example command that could be used to launch training for 1000
steps, assuming that appropriate data and metadata files are stored at
`${TFRECORD}` and `${DATASET_INFO}`:
```bash
python seq2species/run_training.py --train_files ${TFRECORD}
--metadata_path ${DATASET_INFO} --hparams 'train_steps=1000'
--logdir $HOME/seq2species
```
This will output [TensorBoard
summaries](https://www.tensorflow.org/guide/summaries_and_tensorboard), [TensorFlow
checkpoints](https://www.tensorflow.org/guide/variables#checkpoint_files), Seq2LabelModelInfo and
Seq2LabelExperimentMeasures metadata to the logdir `$HOME/seq2species`.

### Preprocessed Seq2Species Data

We have provided preprocessed data based on 16S reference sequences from the
[NCBI RefSeq Targeted Loci
Project](https://www.ncbi.nlm.nih.gov/refseq/targetedloci/) in a Seq2Species
bucket on Google Cloud Storage. After installing the
[Cloud SDK](https://cloud.google.com/sdk/install),
one can download those data (roughly 25 GB) to a local directory `${DEST}` using
the `gsutil` command:
```bash
BUCKET=gs://brain-genomics-public/research/seq2species
mkdir -p ${DEST}
gsutil -m cp ${BUCKET}/* ${DEST}
```

To check if the copy has completed successsfully, check the `${DEST}` directory:
```bash
ls -1 ${DEST}
```
which should produce:
```bash
ncbi_100bp_revcomp.dataset_info.pbtxt
ncbi_100bp_revcomp.tfrecord
```

The following command can be used to train a copy of one of our best-perfoming
deep neural network models for 100 base pair (bp) data. This command also
illustrates how to set hyperparameter values explicitly from the commandline.
The file `configuration.py` provides a full list of hyperparameters, their descriptions,
and their default values. Additional flags are described at the top of
`run_training.py`.
```bash
python seq2species/run_training.py \
--num_filters 3 \
--noise_rate 0.04 \
--train_files ${DEST}/ncbi_100bp_revcomp.tfrecord \
--metadata_path ${DEST}/ncbi_100bp_revcomp.dataset_info.pbtxt \
--logdir $HOME/seq2species \
--hparams 'filter_depths=[1,1,1],filter_widths=[5,9,13],grad_clip_norm=20.0,keep_prob=0.94017831318,
lr_decay=0.0655052811,lr_init=0.000469689635793,lrelu_slope=0.0125376069918,min_read_length=100,num_fc_layers=2,num_fc_units=2828,optimizer=adam,optimizer_hp=0.885769367218,pointwise_depths=[84,58,180],pooling_type=avg,train_steps=3000000,use_depthwise_separable=true,weight_scale=1.18409526348'
```

### Visualization

[TensorBoard](https://github.com/tensorflow/tensorboard) can be used to
visualize training curves and other metrics stored in the summary files produced
by `run_training.py`. Use the following command to launch a TensorBoard instance
for the example model directory `$HOME/seq2species`:
```bash
tensorboard --logdir=$HOME/seq2species
```

## Contact

Any issues with the Seq2Species framework should be filed with the
[TensorFlow/models issue tracker](https://github.com/tensorflow/models/issues).
Questions regarding Seq2Species capabilities can be directed to
[seq2species-interest@google.com](mailto:seq2species-interest@google.com). This
code is maintained by [@apbusia](https://github.com/apbusia) and
[@depristo](https://github.com/depristo).
