# Copyright 2018 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Defines hyperparameter configuration for ConvolutionalNet models.

Specifically, provides methods for defining and initializing TensorFlow
hyperparameters objects for a convolutional model as defined in:
seq2species.build_model
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import json

def parse_hparams(hparam_values='', num_filters=1):
  """Initializes TensorFlow hyperparameters object with default values.

  In addition, default hyperparameter values are overwritten with the specified
  ones, where necessary.

  Args:
    hparam_values: comma-separated string of name=value pairs for setting
      particular hyperparameters.
    num_filters: int; number of filters in the model.
      Must be fixed outside of hyperparameter/study object as Vizier does not
      support having inter-hyperparameter dependencies.

  Returns:
    tf.contrib.training.Hparams object containing the model's hyperparameters.
  """
  hparams = {}

  # Specify model architecture option.
  hparams['use_depthwise_separable'] = True

  # Specify number of model parameters.
  hparams['filter_widths'] = [3] * num_filters
  hparams['filter_depths'] = [1] * num_filters
  hparams['pointwise_depths'] = [64] * num_filters
  hparams['num_fc_layers'] = 2
  hparams['num_fc_units'] = 455
  hparams['min_read_length'] = 100
  hparams['pooling_type'] = 'avg'

  # Specify activation options.
  hparams['lrelu_slope'] = 0.0  # Negative slope for leaky relu.

  # Specify training options.
  hparams['keep_prob'] = 1.0
  hparams['weight_scale'] = 1.0
  hparams['grad_clip_norm'] = 20.0
  hparams['lr_init'] = 0.001
  hparams['lr_decay'] = 0.1
  hparams['optimizer'] = 'adam'
  # optimizer_hp is decay rate for 1st moment estimates for ADAM, and
  # momentum for SGD.
  hparams['optimizer_hp'] = 0.9
  hparams['train_steps'] = 400000

  # Overwrite defaults with specified values.
  for key, val in parse_hparams_string(hparam_values).items():
    hparams[key] = val
  return hparams

def parse_hparams_string(hparam_string):
  '''
    Return dict with the hyperparameters
  '''
  hparams = {}
  hparam_parts = hparam_string.split('=')
  hparam_parts = map(lambda s: s.rsplit(',', 1), hparam_parts)
  # [['filter_depths'], ['[1,1,1]', 'filter_widths'], ['[5,9,13]', 'grad_clip_norm'], ['20.0']]
  flat_list = []
  for list in hparam_parts:
    for item in list:
      flat_list.append(item)
  keyvalue_list = [flat_list[i:i + 2] for i in range(0, len(flat_list), 2)]
  # [['filter_depths', '[1,1,1]'], ['filter_widths', '[5,9,13]'], ['grad_clip_norm', '20.0']]
  for key, value in keyvalue_list:
    if value.startswith('['):  # It's an array
      value = json.loads(value)
      hparams[key] = value
    else: # It's a number or string
      if '.' in value: # likely a float
        try:  # it's a float
          hparams[key] = float(value)
        except ValueError: # it's a string
          hparams[key] = value
      else:  # it's a int or string
        try: # an int
          hparams[key] = int(value)
        except ValueError: # it's a string
          hparams[key] = value
  return hparams
