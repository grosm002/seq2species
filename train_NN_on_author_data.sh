#!/bin/bash
export TF_CPP_MIN_LOG_LEVEL=2
python -B run_training.py \
--num_filters 3 \
--noise_rate 0.04 \
--train_files author_data/ncbi_100bp_revcomp.tfrecord \
--metadata_path author_data/ncbi_100bp_revcomp.dataset_info.pbtxt \
--logdir logdir \
--hparams 'filter_depths=[1,1,1],filter_widths=[5,9,13],grad_clip_norm=20.0,keep_prob=0.94017831318,lr_decay=0,lr_init=0.1,lrelu_slope=0,min_read_length=100,num_fc_layers=2,num_fc_units=2828,optimizer=adam,optimizer_hp=0.885769367218,pointwise_depths=[84,58,180],pooling_type=avg,train_steps=3000000,use_depthwise_separable=true,weight_scale=1.18409526348'

# Orig hyperparameters
# --hparams 'filter_depths=[1,1,1],filter_widths=[5,9,13],grad_clip_norm=20.0,keep_prob=0.94017831318,lr_decay=0.0655052811,lr_init=0.000469689635793,lrelu_slope=0.0125376069918,min_read_length=100,num_fc_layers=2,num_fc_units=2828,optimizer=adam,optimizer_hp=0.885769367218,pointwise_depths=[84,58,180],pooling_type=avg,train_steps=3000000,use_depthwise_separable=true,weight_scale=1.18409526348'
